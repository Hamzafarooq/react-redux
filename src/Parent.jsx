import React from 'react'
import Child from './Child'

const Parent = () => {
    
  return (
    <div>
    parent =
        <h2>This is a parent Component of a context Hooks</h2>
       
    </div>
  )
}

export default Parent
