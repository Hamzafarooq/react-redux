import React, { useReducer } from 'react'
import '../usereducerhook/UseReducerHook.css'
import reducer from './reducer';
const UseReducerHook = () => {
    const initialValue = 0;
 const [count , dispatch] = useReducer(reducer,initialValue)
  return (
    <div>
      <div>
      <button onClick={() => dispatch({type : "Inc"})}>Increment</button>
      <span>{count}</span>
      <button  onClick={() => dispatch({type: "Dec"})}>Decrement</button>
       
      </div>
    </div>
  )
}

export default UseReducerHook
