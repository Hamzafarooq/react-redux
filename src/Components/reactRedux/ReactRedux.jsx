import React from "react";
import "./ReactRedux.css";
import ipimg from "../Images/iphone11.jpg";
import shopcart from "../Images/shop.png";
const ReactRedux = () => {
  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-12">
            <h4 className="text-center">Add To Cart</h4>
          </div>
<div className="col-12">
<img src={shopcart} className="img-fluid shopimg" />
<div className="count">0</div>
    </div>
          <div className="col">
            <img src={ipimg} alt="" className="img-fluid mobileimg" />
            <h4 className="ml-4">Iphone 14 pro max</h4>
            <p className="ml-4">Price : 100000</p>
            <button className="addbtn">Add to cart</button>
          </div>
        </div>
      </div>
    </>
  );
};

export default ReactRedux;
