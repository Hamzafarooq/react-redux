import { useEffect, useState } from "react";
import "./ApiCall.css";
import axios from "./../../axios"



const ApiCall = () => {
  const [myData, setMyData] = useState([]);
  const [isError, setIsError] = useState("");

    const API = "https://jsonplaceholder.typicode.com"

//   useEffect(() => {
//     axios.get("https://jsonplaceholder.typicode.com/posts")
//       .then((res) => {
//         setMyData(res.data);
//         console.log(res.data);
//       })
//       .catch((err) => {
//           setIsError(err.message)
//         console.log(err.message);
//       });
//   }, []);
const getApiData = async (url) => {
    try {
        const res = await axios.get(url)
        setMyData(res.data)
    } catch (err) {
        setIsError(err.message)
    }
   
}
useEffect(  () => {
   getApiData(`${API}/posts`)
  }, []);
  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col">
            <h3>This is the component of Api call</h3>
            <div className=" divforapidat">
            {isError !== 0 && <h2> {isError}</h2>}
            {myData.slice(0, 20).map((item, ind) => {
              const { id, title, body } = item;
              return (
                <>
                <div className="border w-25 m-4">
                  <div className="text-center ">
                    <h3>{id}</h3>
                  </div>
                  <div className="text-center ">
                    <h4>{title.slice(0,30)}</h4>
                  </div>

                  <div className="text-center ">
                    <p>{body.slice(0,150)}</p>
                    </div>
                    </div>
                    </>
                    );
                })}
                </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ApiCall;
