
import './App.css';
import Child from './Child';
import UseReducerHook from './Components/usereducerhook/UseReducerHook';

import Parent from './Parent';
import ApiCall from './Components/ApiCall/ApiCall';
import ReactRedux from './Components/reactRedux/ReactRedux';
import Home from './Components/Home';
import HomeContainer from './container/HomeContainer'


function App() {

  
  return (
    <>
        <HomeContainer />
        
    </>
  );
}

export default App;
