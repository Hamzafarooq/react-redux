import {connect} from 'react-redux'
import Home from './../Components/Home';

import {addToCart,removeToCart}  from './../Services/actions/Action';


const mapStateToProps= state=>({
  cardData:state.cardItems
})

const mapDispatchToProps= dispatch=>({
    addCardHandler:data=>{dispatch(addToCart(data))},
    removeCardHandler:data=>{dispatch(removeToCart(data))}
})

export default connect(mapStateToProps,mapDispatchToProps)(Home)