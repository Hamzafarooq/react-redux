import React from 'react'
import { useContext } from 'react'
import { AppContext } from './Components/usecontexthooks/UseContextHooks'

const Child = () => {
    
    const userData = useContext(AppContext);
  return (
    <div>
    child =
        <h1>This is the Child Component using context api for passing data here</h1>
       <p>My Name is {userData.name} and my Age IS a {userData.age} </p>
    </div>
  )
}

export default Child
